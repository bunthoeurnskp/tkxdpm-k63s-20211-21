package view;

import entity.Station;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Optional;

public class StationComponent {
    ManageStationScreen manageStationScreen=null;
    private FXMLLoader loader;
    @FXML
    private AnchorPane content ;
    @FXML
    private Label name,address,slots,bikes;
    @FXML
    private Button edit,delete;
    private Station station;
    public StationComponent(Station station,ManageStationScreen parent) throws IOException {
        this.loader = new FXMLLoader(getClass().getResource("../fxml/station_component.fxml"));
        // Set this class as the controller
        this.loader.setController(this);
        this.station=station;
        this.content = (AnchorPane) loader.load(    );
        manageStationScreen=parent;
        name.setText(station.getStationname());
        address.setText(station.getStationaddress());
        slots.setText(String.valueOf(station.getNumberofemptydocks()));
        bikes.setText(String.valueOf(station.getNumberofbikes()));
        edit.setOnMouseClicked(e -> {
            manageStationScreen.setSelectedItem(station);
        });
        delete.setOnMouseClicked(e->{
            manageStationScreen.deleteItem(station);
        });
    }

    public AnchorPane getContent() {
        return content;
    }
}
