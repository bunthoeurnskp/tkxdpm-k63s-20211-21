package controller;

import dao.ConnectDatabaseDAO;
import entity.Station;
import javafx.scene.control.Alert;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ManageStationController {
    private Connection connection;

    public ManageStationController() {
        connection= ConnectDatabaseDAO.getConnection();
    }

    public String addStation(String name,String address,String slots){
        if(name.equals("")) return "Ten khong duoc de trong";
        if(address.equals("")) return "Dia chi khong duoc de trong";
        int s=-1;
        try {
            s=Integer.parseInt(slots);
        }catch (Exception e){
            return "Slot trong phai la so nguyen lon hon 0";
        }
        Station station=new Station(name,address,s);
       try{
           String sql="insert into station(stationname,stationaddress,numberofemptydocks) " +
                   "values(?,?,?)";
           PreparedStatement statement=connection.prepareStatement(sql);
           statement.setString(1,station.getStationname());
           statement.setString(2,station.getStationaddress());
           statement.setInt(3,station.getNumberofemptydocks());
           int rowaffected=statement.executeUpdate();
           if(rowaffected==1) return "SUCCESS";
           else return "Da xay ra loi";
       }catch (Exception e){
           e.printStackTrace();
           return "Da xay ra loi";
       }
    }
    public boolean deleteStation(Station station){
        try{
            String sql="delete from station" +
                    " where id=?";
            PreparedStatement statement=connection.prepareStatement(sql);
            statement.setInt(1,station.getId());
            int rowaffected=statement.executeUpdate();
            if(rowaffected==1) return true;
            else return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    public List<Station> getStations() throws SQLException {
        List<Station> stations = new ArrayList<>();
        String sql = "select * from station";

        PreparedStatement preStatement = null;
        try {
            preStatement = this.connection.prepareStatement(sql);
            ResultSet result = preStatement.executeQuery();
            while (result.next()) {
                Station sta = new Station();
                sta.setId(result.getInt("id"));
                sta.setStationname(result.getString("stationname"));
                sta.setStationaddress(result.getString("stationaddress"));
                sta.setNumberofemptydocks(result.getInt("numberofemptydocks"));
                stations.add(sta);
                sql = "Select count(*) from bike where station=" + sta.getId() + " and status=1";
                preStatement = this.connection.prepareStatement(sql);
                ResultSet rs = preStatement.executeQuery();
                rs.next();
                sta.setNumberofbikes(rs.getInt(1));
                rs.close();
            }
            result.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            preStatement.close();
        }
        return stations;
    }
    public List<Station> getStationsByName(String name) throws SQLException {
        List<Station> stations = new ArrayList<>();
        String sql = "select * from station where stationname like ?";

        PreparedStatement preStatement = null;
        try {
            preStatement = this.connection.prepareStatement(sql);
            preStatement.setString(1,"%"+name+"%");
            ResultSet result = preStatement.executeQuery();
            while (result.next()) {
                Station sta = new Station();
                sta.setId(result.getInt("id"));
                sta.setStationname(result.getString("stationname"));
                sta.setStationaddress(result.getString("stationaddress"));
                sta.setNumberofemptydocks(result.getInt("numberofemptydocks"));
                stations.add(sta);
                sql = "Select count(*) from bike where station=" + sta.getId() + " and status=1";
                preStatement = this.connection.prepareStatement(sql);
                ResultSet rs = preStatement.executeQuery();
                rs.next();
                sta.setNumberofbikes(rs.getInt(1));
                rs.close();
            }
            result.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            preStatement.close();
        }
        return stations;
    }
    public boolean updateStation(Station station){
        try{
            String sql="update station " +
                    " set stationname=?,stationaddress=?,numberofemptydocks=? " +
                    "where id=?";
            PreparedStatement statement=connection.prepareStatement(sql);
            statement.setString(1,station.getStationname());
            statement.setString(2,station.getStationaddress());
            statement.setInt(3,station.getNumberofemptydocks());
            statement.setInt(4,station.getId());
            int rowaffected=statement.executeUpdate();
            if(rowaffected==1) return true;
            else return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
