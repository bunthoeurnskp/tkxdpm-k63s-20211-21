package entity;
import java.util.Date;

public class RentInfo {

    int id;
    Bike bike;
    Date rent_time;

    public RentInfo(int id, Bike bike, Date rent_time) {
        this.id = id;
        this.bike = bike;
        this.rent_time = rent_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bike getBike() {
        return bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public Date getRent_time() {
        return rent_time;
    }

    public void setRent_time(Date rent_time) {
        this.rent_time = rent_time;
    }
}
