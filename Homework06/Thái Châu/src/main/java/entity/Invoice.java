package entity;

//Singleton class
public class Invoice {

    private static Invoice invoice = null;

    public int price;
    public String type;
    public String rentTime;
    public String returnTime;
    public String bikeId;
    public int stationId;

    public static Invoice Invoice() {
        if(invoice == null) {
            invoice = new Invoice();
        }
        return invoice;
    }

    public void clearInvoice() {
        this.price = 0;
        this.returnTime = "";
        this.rentTime = "";
        this.type = "";
        this.bikeId = "";
        this.stationId = 0;
    }
}
