package test;

import controller.PaymentController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PaymentControllerTest {
    private PaymentController paymentController;

    @BeforeEach
    void setUp() {
        paymentController = new PaymentController();
    }

    @ParameterizedTest
    @CsvSource({
            "kscq_tkxd_group21,true",
            "Group 21,false",
            "%ksc_abc,false",
            ",false"
    })
    void validateCardNumber(String str, boolean expected) {
        boolean isValid = paymentController.validateCardNumber(str);
        Assertions.assertEquals(isValid, expected);
    }

    @ParameterizedTest
    @CsvSource({
            "kscq_tkxd_group21,false",
            "Group 21,true",
            "%ksc_abc,false",
            ",false"
    })
    void validateName(String str, boolean expected) {
        boolean isValid = paymentController.validateName(str);
        Assertions.assertEquals(isValid, expected);
    }

    @ParameterizedTest
    @CsvSource({
            "817,true",
            "8 17,false",
            "%34,false",
            "12b,false",
            ",false"
    })
    void validateCvv(String cvv, boolean expected) {
        boolean isValid = paymentController.validateCvv(cvv);
        Assertions.assertEquals(isValid, expected);
    }

    @ParameterizedTest
    @CsvSource({
            "11/25,true",
            "1125,false",
            "11 25,false",
            "a2/bc,false",
            "$@/#7,false",
            ",false"
    })
    void validateExpirationDate(String date, boolean expected) {
        boolean isValid = paymentController.validateExpirationDate(date);
        Assertions.assertEquals(isValid, expected);
    }
}