package controller;

import java.io.IOException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import dao.ConnectDatabaseDAO;
import entity.Invoice;
import exception.InvalidCardException;
import exception.PaymentException;
import exception.UnrecognizedException;
import entity.CreditCard;
import entity.PaymentTransaction;
import subsystem.InterbankInterface;
import subsystem.InterbankSubsystem;

public class PaymentController {

    private Connection connection;

    private CreditCard card;

    private InterbankInterface interbank;

    Invoice invoice = Invoice.Invoice();

    public PaymentController() {
        connection= ConnectDatabaseDAO.getConnection();
    }

    private String getExpirationDate(String date) throws InvalidCardException {
        String[] strs = date.split("/");
        if (strs.length != 2) {
            throw new InvalidCardException();
        }

        String expirationDate = null;
        int month = -1;
        int year = -1;

        try {
            month = Integer.parseInt(strs[0]);
            year = Integer.parseInt(strs[1]);
            if (month < 1 || month > 12 || year < Calendar.getInstance().get(Calendar.YEAR) % 100 || year > 100) {
                throw new InvalidCardException();
            }
            expirationDate = strs[0] + strs[1];

        } catch (Exception ex) {
            throw new InvalidCardException();
        }

        return expirationDate;
    }

    public Map<String, String> payOrder(int amount, String contents, String cardNumber, String cardHolderName,
                                        String expirationDate, String securityCode) {
        Map<String, String> result = new Hashtable<String, String>();
        result.put("RESULT", "PAYMENT FAILED!");
        try {
            this.card = new CreditCard(cardNumber, cardHolderName, Integer.parseInt(securityCode),
                    getExpirationDate(expirationDate));

            this.interbank = new InterbankSubsystem();
            PaymentTransaction transaction = interbank.payOrder(card, amount, contents);

            result.put("RESULT", "PAYMENT SUCCESSFUL!");
            result.put("MESSAGE", "You have succesffully paid the order!");


            try{
                //Cập nhật station, status của bike trong database
                String sql="update bikes set station = ?, status = 1 where id = ?";
                PreparedStatement statement1=connection.prepareStatement(sql);
                statement1.setInt(1,invoice.stationId);
                statement1.setString(2,invoice.bikeId);
                statement1.executeUpdate();

                //Xóa dữ liệu ở bảng rent_infos
                sql = "DELETE FROM rent_infos WHERE bike_id = ?";
                PreparedStatement statement2=connection.prepareStatement(sql);
                statement2.setString(1,invoice.bikeId);
                statement2.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e);
            }
            invoice.clearInvoice();

        } catch (PaymentException | UnrecognizedException ex) {
            result.put("MESSAGE", ex.getMessage());
        }
        return result;
    }

    public boolean validateCardInfo(String cardNumber, String cardHolderName, String expirationDate, String securityCode)  {
        return validateCardNumber(cardNumber) && validateCvv(securityCode) && validateExpirationDate(expirationDate) && validateName(cardHolderName);
    }

    public boolean validateName(String str) {
        if(str == null || str.isEmpty()) return false;
        for (char ch : str.toCharArray()) {
            if ( ch == ' ') {
                continue;
            }
            if ( !Character.isLetter(ch) ) {
                if(!Character.isDigit(ch)) return false;
            }
        }
        return true;
    }

    public boolean validateCardNumber(String str) {
        if(str == null || str.isEmpty()) return false;
        for (char ch : str.toCharArray()) {
            if ( ch == '_') {
                continue;
            }
            if (!Character.isLetter(ch)) {
                if(!Character.isDigit(ch)) return false;
            }
        }

        return true;
    }

    public boolean validateCvv(String cvv) {
        if(cvv == null || cvv.isEmpty()) return false;
        if(cvv.length() !=3) return false;

        for (char ch : cvv.toCharArray()) {
            if ( !Character.isDigit(ch) ) {
                return false;
            }
        }
        return true;
    }

    public boolean validateExpirationDate(String date) {
        if(date == null || date.isEmpty()) return false;
        if(date.length() != 5) return false;

        if(date.charAt(2) != '/') return false;

        for(int i=0; i<5; i++) {
            if(i!=2) {
                if(!Character.isDigit(date.charAt(i))) return false;
            }
        }
        return true;
    }
}