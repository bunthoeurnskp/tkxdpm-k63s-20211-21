package controller;

import dao.ConnectDatabaseDAO;
import entity.Invoice;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.Date;

import static utils.Utils.getCurrency;

public class ReturnBikeController {
    private Connection connection;
    private Date now = new Date();
    Invoice invoice = Invoice.Invoice();

    public ReturnBikeController() {
        connection= ConnectDatabaseDAO.getConnection();
    }

    public ArrayList createInvoice() {
        ArrayList<String> list = new ArrayList<>();
        try {
            PreparedStatement p = connection.prepareStatement("SELECT * FROM rent_infos");
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                String returnTime = dateToString(now);
                String rentTime = dateToString(rs.getTimestamp("rent_time"));
                long fee = calculateFee(rs.getTimestamp("rent_time"), rs.getInt("bike_type"));
                String price = getCurrency(fee);

                list.add(Integer.toString(rs.getInt("bike_id")));
                list.add(Integer.toString(rs.getInt("bike_type")));
                list.add(rentTime);
                list.add(returnTime);
                list.add(price);

                //Save to entity Invoice
                invoice.bikeId = list.get(0);
                invoice.type = list.get(1);
                invoice.rentTime = list.get(2);
                invoice.returnTime = list.get(3);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    private long calculateFee(Date rentTime, int type ) {
        //Tinh phi
        long fee;
        long duration  = now.getTime() - rentTime.getTime();
        long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
        if(diffInMinutes <= 10) fee = 0;
        else if(diffInMinutes > 10 && diffInMinutes <=30) fee = 10000;
        else {
            if((diffInMinutes-30)%15 != 0) fee = 10000 + ((diffInMinutes-30)/15 + 1)*3000;
            else fee = 10000 + ((diffInMinutes-30)/15)*3000;
        }
        if(type != 1) fee*=1.5;

        invoice.price = (int) fee;
        return fee;
    }

    private String dateToString(Date date) {
        String pattern = "MM/dd/yyyy HH:mm:ss";
        DateFormat df = new SimpleDateFormat(pattern);
        String rs = df.format(date);
        return rs;
    }


}
