package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ManageBikeController;

class validateWeightTest {

	private ManageBikeController manageBike;
	@BeforeEach
	void setUp() throws Exception {
		manageBike = new ManageBikeController();
	}

	@ParameterizedTest
	@CsvSource({
		"123, true",
		"-123, false",
		"asdf1, false",
		"12 42, false",
		"'     ', false",
		",false"
	})

	void test(String weight, boolean expect) {
		boolean isValid = manageBike.validateWeight(weight);
		assertEquals(isValid, expect);
	}
}
