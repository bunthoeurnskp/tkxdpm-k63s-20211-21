package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ManageBikeController;


class validateNameTest {

	private ManageBikeController manageBike;
	@BeforeEach
	void setUp() throws Exception {
		manageBike = new ManageBikeController();
	}

	@ParameterizedTest
	@CsvSource({
		"xe dap 1, true",
		"xe đạp 1, true",
		"xe đạp honda g-mk1, true",
		"$#12fa, false",
		"132 @#, false",
		"()ADa, false",
		"() xe 1, false",
		"'     ', false",
		",false"
	})

	void test(String name, boolean expect) {
		boolean isValid = manageBike.validateName(name);
		assertEquals(isValid, expect);
	}

}
