package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ManageBikeController;

class validateDateTest {

	private ManageBikeController manageBike;
	@BeforeEach
	void setUp() throws Exception {
		manageBike = new ManageBikeController();
	}

	@ParameterizedTest
	@CsvSource({
		"11/11/2022, 11/11/2021, false",
		"11/12/2021, 11/11/2021, false",
		"12/11/2021, 11/11/2021, false",
		"11/11/2021, 11/11/2021, true",
		"11/11/2020, 11/11/2021, true",
		"11/10/2021, 11/11/2021, true",
		"10/11/2021, 11/11/2021, true",
		"          , 11/11/2021, false",
		"          ,           , false"
	})

	void test(Date date, Date currDate, boolean expect) {
		boolean isValid = manageBike.validateDate(date, currDate);
		assertEquals(isValid, expect);
	}

}
