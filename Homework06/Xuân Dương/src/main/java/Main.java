import dao.ConnectDatabaseDAO;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.Connection;

import controller.ManageBikeController;



public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Connection connection=ConnectDatabaseDAO.getConnection();
        //tạo cửa sổ mới và load fxml file
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/bike_fxml/manageBike.fxml"));
        Scene scene=new Scene(root);
        primaryStage.setResizable(true);
        primaryStage.setScene(scene);
        primaryStage.show();
        ManageBikeController manageBikeController = new ManageBikeController();
		manageBikeController.getAllBike();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    public static void main(String[] args) throws IOException {
        launch(args);
    }
}