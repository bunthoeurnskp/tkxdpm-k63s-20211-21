package entity;

import java.math.BigDecimal;
import java.util.Date;

public class Bike {
    protected int id;
    protected String name;
    protected String type;
    protected int weight;
    protected String license;
    protected Date manufacturingdate;
    protected String producer;
    protected int cost;

    public Bike(int id,String name, String type, int weight, String license, Date manufacturingdate, String producer, int cost) {
        this.id=id;
        this.name = name;
        this.type = type;
        this.weight = weight;
        this.license = license;
        this.manufacturingdate = manufacturingdate;
        this.producer = producer;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Date getManufacturingdate() {
        return manufacturingdate;
    }

    public void setManufacturingdate(Date manufacturingdate) {
        this.manufacturingdate = manufacturingdate;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
