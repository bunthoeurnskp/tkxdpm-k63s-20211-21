package view;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import controller.ManageBikeController;
import entity.Bike;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class ManageBikeScreen implements Initializable{

	@FXML
	private TableView<Bike> table;
	@FXML
	private TableColumn<Bike, String> name;
	@FXML
	private TableColumn<Bike, String> type;
	@FXML
	private TableColumn<Bike, Integer> weight;
	@FXML
	private TableColumn<Bike, String> license;
	@FXML
	private TableColumn<Bike, Date> manufacturingdate;
	@FXML
	private TableColumn<Bike, String> producer;
	@FXML
	private TableColumn<Bike, BigDecimal> cost;
	
	ManageBikeController managebike = new ManageBikeController();
	List<Bike> bikes = new ArrayList<Bike>();
	private Stage stage;
	private Scene scene;
	private Parent root;
	
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    	name.setCellValueFactory(new PropertyValueFactory<Bike, String>("name"));
    	type.setCellValueFactory(new PropertyValueFactory<Bike, String>("type"));
    	weight.setCellValueFactory(new PropertyValueFactory<Bike, Integer>("weight"));
    	license.setCellValueFactory(new PropertyValueFactory<Bike, String>("license"));
    	manufacturingdate.setCellValueFactory(new PropertyValueFactory<Bike, Date>("manufacturingdate"));
    	producer.setCellValueFactory(new PropertyValueFactory<Bike, String>("producer"));
    	cost.setCellValueFactory(new PropertyValueFactory<Bike, BigDecimal>("cost"));
    	
    	table.getItems().setAll(managebike.getAllBike());
    }
	
	@FXML
	public void addBikeScreen(MouseEvent event)throws IOException, InterruptedException, SQLException{
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/bike_fxml/addBike.fxml"));
		root = loader.load();
		
		//Get the source of the event and then cast it to a node
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
