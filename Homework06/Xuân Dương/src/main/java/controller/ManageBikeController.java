package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import dao.ConnectDatabaseDAO;
import entity.Bike;
import entity.Station;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ManageBikeController {

	public static final String NUM_ALPHABET_REGEX = "[a-zA-Z_0-9\\-_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢ"
			+ "ẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈ" 
			+ "ỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$";
	public static final String NO_SPECIAL_REGEX = "^[a-zA-Z_0-9_\\,\\.\\/\\_\\- ]+$";
	
	private Connection connection = ConnectDatabaseDAO.getConnection();
	private ResultSet rs;
	private String query;
	Pattern pattern;

	
	public List<Bike> getAllBike() {
		List<Bike> bike = new ArrayList<Bike>();
		try {
			Statement statement = connection.createStatement();
			query = "SELECT * FROM bikes";
			rs = statement.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String type = rs.getString("type");
				int weight = rs.getInt("weight");
				String license = rs.getString("license");
				Date manufacturingdate = rs.getDate("manufacturingdate");
				String producer = rs.getString("producer");
				int cost = rs.getInt("cost");
				Bike a = new Bike(id, name, type, weight, license, manufacturingdate, producer, cost);
				bike.add(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return bike;
	}

	public List<Station> getStations() throws SQLException {
		List<Station> stations = new ArrayList<>();
		String sql = "select * from stations";

		PreparedStatement preStatement = null;
		try {
			preStatement = this.connection.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				Station sta = new Station();
				sta.setId(result.getInt("id"));
				sta.setStationname(result.getString("stationname"));
				sta.setStationaddress(result.getString("stationaddress"));
				sta.setNumberofemptydocks(result.getInt("numberofempltydocks"));
				stations.add(sta);
				System.out.println(sta.getId());
			}
			result.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			preStatement.close();
		}
		return stations;
	}
	

	public void addBike(String name, int station_id, String type, int weight, String license, Date mdate, String producer, int cost) throws SQLException {
		java.sql.Date sqlDate = new java.sql.Date(mdate.getTime());

		query = "INSERT INTO ecobike.bikes(id, station_id, name, type, weight, license, manufacturingdate, producer, cost)"
				+ " VALUES ('" + 0 + "','" + station_id + "', '" + name + "', '" 
				+ type + "', '" + weight + "', '" + license + "', '"
				+ sqlDate + "', '" + producer + "', '" + cost + "')";
		try {
			Statement st = connection.createStatement();
			st.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean checkDuplicateName(String nameOfBike) {
		List<Bike> bikelist = getAllBike();
		System.out.println(nameOfBike + "\nList of Bike:");
		int numOfBikes = bikelist.size();
		Bike b;
		boolean result = true;
		for(int i = 0; i < numOfBikes; i++) {
			b = bikelist.get(i);
			System.out.println(b.getName());
			if (nameOfBike.equalsIgnoreCase(b.getName())) { 
				result = false;
				break;
			}
		}
		return result;
	}
	
	
	public String processBikeInfo(String name, String weight, String license, Date date, Date currDate, String cost) {
		if (!validateName(name)) {
			return "Tên không hợp lệ. ";
		}
		if (!validateWeight(weight)) {
			return "Khối lượng không hợp lệ. ";
		}
		if (!validateLicense(license)) {
			return "Biển số xe không hợp lệ. ";
		}
		if (!validateDate(date, currDate)) {
			return "Ngày sản xuất không hợp lệ. ";
		}
		if (!validateCost(cost)) {
			return "Gía thành không hợp lệ. ";
		}
		return "valid_info";
	}

	public boolean validateName(String name) {
		boolean result = true;

		if (name != null && !name.isBlank()) {
			pattern = Pattern.compile(NUM_ALPHABET_REGEX);
			result = pattern.matcher(name).matches();
		} else
			result = false;

		return result;
	}

	public boolean validateWeight(String weight) {
		if (weight != null) {
			int w;
			try {
				w = Integer.parseInt(weight);
			} catch (NumberFormatException e) {
				return false;
			}
			if (w < 0)
				return false;
		} else
			return false;

		return true;
	}

	public boolean validateLicense(String license) {
		boolean result = true;

		if (license != null && !license.isBlank()) {
			pattern = Pattern.compile(NO_SPECIAL_REGEX);
			result = pattern.matcher(license).matches();
		} else
			result = false;
		
		return result;
	}

	public boolean validateDate(Date manufactureDate, Date currentDate) {
		
		if(manufactureDate != null) {
			if(manufactureDate.compareTo(currentDate) <= 0) {
				return true;
			}	
		} else return false;
		return false;
	}

	public boolean validateCost(String cost) {
		if (cost != null) {
			int c;
			try {
				c = Integer.parseInt(cost);
			} catch (NumberFormatException e) {
				return false;
			}
			if (c < 0)
				return false;
		} else
			return false;

		return true;
	}

}
