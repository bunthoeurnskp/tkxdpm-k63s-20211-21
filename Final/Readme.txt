Trong suốt 16 tuần học nhóm đã hoàn thành các bài tập tuần và bài tập lớn.
Bài tập lớn gồm các chức năng: xem danh sách bãi xe, xem chi tiết bãi xe, thuê xe, trả xe, quản lí bãi xe, quản lí xe.
Đánh giá kết quả của cá nhân vào kết quả chung của nhóm:
 - Bùi Trường Giang: 20%. Ngoài thực hiện các bài tập và usecase cá nhân thì còn thực hiện ghép code cho cả nhóm. TechLead của nhóm.
 - Nguyễn Thuý Linh: 20%. Tổng hợp làm báo cáo cho nhóm.
 - Mạc Trung Tình: 20%. Tổng hợp làm báo cáo cho nhóm.
 - Yorn Bunthoeurn: 20%.
 - Bùi Xuân Dương: 20%.