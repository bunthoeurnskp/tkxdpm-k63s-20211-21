package view;

import controller.ManageBikeController;
import entity.Bike;
import entity.Station;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class AddBikeScreen implements Initializable {
	@FXML
	private TextField name;
	@FXML
	private ChoiceBox<String> type;
	@FXML
	private TextField license;
	@FXML
	private TextField pin;
	@FXML
	private ChoiceBox<String> station;
	@FXML
	private TextField cost;

	private String[] bikeTypes = { "Xe đạp thường", "Xe điện", "Xe đạp đôi" };
	private Alert a = new Alert(AlertType.ERROR);
	private Stage stage;
	private Scene scene;
	private Parent root;

	private ManageBikeController managebike = new ManageBikeController();
	private List<Bike> bike = managebike.getAllBike();
	private HashMap<String,Integer> mymap;
	private HashMap<String,Integer> stationmap;
	private List<String> stationoption;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		type.getItems().addAll(bikeTypes);
		type.setValue("Xe đạp thường");
		mymap=new HashMap<>();
		stationmap=new HashMap<>();
		stationoption=new ArrayList<>();
		for(int i=0;i<3;i++){
			mymap.put(bikeTypes[i],i+1 );
		}
		try {
			for(Station station:managebike.getStations()) {
				stationmap.put(station.getStationname(),station.getId());
				stationoption.add(station.getStationname());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		station.getItems().addAll(stationoption);
	}

	@FXML
	public void returnToPrevious(MouseEvent event) throws IOException, InterruptedException, SQLException {

		Stage stage=(Stage)((Node)event.getSource()).getScene().getWindow();
		Parent root= FXMLLoader.load(getClass().getResource("../fxml/manageBike.fxml"));
		Scene scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	@FXML
	public void AddBikeToDb(MouseEvent event) throws IOException, SQLException, InterruptedException {

			String message = managebike.processBikeInfo( license.getText(), cost.getText());

			if (message.equals("valid_info")) {
				int bType = mymap.get(type.getValue());
					String bLicense = license.getText();
					int bpin=pin.getText().equals("")?-1:Integer.parseInt(pin.getText());
					double bCost = Double.parseDouble(cost.getText());

					managebike.addBike(bType,1, bCost,bpin,bLicense,stationmap.get(station.getValue()));


			} else {
				a.setContentText(message);
				a.show();
			}
			returnToPrevious(event);
	}

}
