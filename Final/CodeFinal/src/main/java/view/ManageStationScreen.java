package view;

import controller.ManageStationController;
import entity.Station;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class ManageStationScreen {
    private final ManageStationController controller=new ManageStationController();
    @FXML
    private Button addstation;
    @FXML
    private Button back,update,cancel;
    @FXML
    private TextField keyword,nameupdate,addressupdate,emptyupdate;
    @FXML
    private AnchorPane popup;
    @FXML
    private VBox vbox;
    private List<Station> stationList=null;
    private Station selectedstation=null;
    @FXML
    public void initialize() {
        try{
            stationList=controller.getStations();
        }catch (Exception e){
            e.printStackTrace();
        }
        loaddata();
    }
    public void loaddata(){
        vbox.getChildren().clear();
        for(Station station:stationList){
            try{
                StationComponent stationComponent=new StationComponent(station,this);
                vbox.getChildren().add(stationComponent.getContent());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    public void showAlert(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
    public void setSelectedItem(Station station){
        selectedstation=station;
        nameupdate.setText(station.getStationname());
        addressupdate.setText(station.getStationaddress());
        emptyupdate.setText(String.valueOf(station.getNumberofemptydocks()));
        popup.setVisible(true);
        System.out.println(selectedstation.getId());
    }
    public void deleteItem(Station station){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText(null);
        alert.setContentText("Bạn chắc chắn muốn xóa bãi xe chứ");
        Optional<ButtonType> option = alert.showAndWait();

        if (option.get() == ButtonType.OK) {
            controller.deleteStation(station);
            stationList.remove(station);
            loaddata();
        }
    }
    public void updateStation(Station station){
        boolean res=controller.updateStation(station);
        if(!res) {
            showAlert("Đã xảy ra lỗi");
        }else{
            loaddata();
            popup.setVisible(false);
            showAlert("Cập nhật thành công");
        }
    }
    @FXML public void search(){
        try{
            stationList=controller.getStationsByName(keyword.getText());
        }catch (Exception e){
            e.printStackTrace();
        }
        loaddata();
    }
    @FXML
    public void popupbtn(ActionEvent actionEvent){
        if(actionEvent.getSource()==cancel){
            popup.setVisible(false);
        }else {
            selectedstation.setStationname(nameupdate.getText());
            selectedstation.setStationaddress(addressupdate.getText());
            try {
                selectedstation.setNumberofemptydocks(Integer.parseInt(emptyupdate.getText()));
            }catch (Exception e){
                showAlert("Số chỗ trống phải là số nguyên");
                return;
            }
            updateStation(selectedstation);
        }
    }

    @FXML
    public void changescreen(ActionEvent event) throws IOException {
        Stage stage=(Stage)((Node)event.getSource()).getScene().getWindow();
        Parent root;
        if(event.getSource()==addstation)
        {
            root= FXMLLoader.load(getClass().getResource("../fxml/AddStation.fxml"));
        }
        else if(event.getSource()==back)
        {
            root=FXMLLoader.load(getClass().getResource("../fxml/home.fxml"));
        }
        else root=FXMLLoader.load(getClass().getResource("../fxml/ManageStation2.fxml"));
        Scene scene=new Scene(root);
        stage.setScene(scene);
        stage.show();

    }
}
