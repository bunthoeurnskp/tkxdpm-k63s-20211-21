package view;

import controller.ManageStationController;
import entity.Station;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;

public class AddStationScreen {
    private ManageStationController controller=new ManageStationController();
    @FXML
    private TextField name;
    @FXML
    private TextField address;
    @FXML
    private TextField coord_x;
    @FXML
    private TextField coord_y;
    @FXML
    private TextField emptyslots;
    @FXML
    private Button addstation;
    @FXML
    private Button cancel;
    @FXML

    public void addStation(ActionEvent event) throws IOException {
        Integer x=null,y=null,z=null;
        try {
            z=Integer.valueOf(emptyslots.getText());
        }catch (Exception e){
            showAlert("Số chỗ trống phải là số nguyên");
            return;
        }
        if (z==null){
            showAlert("Số chỗ trống phải là số nguyên");
        }
        else {
            boolean res=controller.addStation(new Station(name.getText(),address.getText(),z));
            if(!res) {
                showAlert("Đã xảy ra lỗi");
                return;
            }else {
                showAlert("Thêm thành công bãi xe");
                changeScreen(event,"../fxml/ManageStation2.fxml");
            }
        }

    }
    public void showAlert(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    @FXML
    public void Cancel(ActionEvent event) throws IOException {
        changeScreen(event,"../fxml/ManageStation2.fxml");
    }
    public void changeScreen(ActionEvent event,String screen) throws IOException {
        Stage stage=(Stage)((Node)event.getSource()).getScene().getWindow();
        Parent root= FXMLLoader.load(getClass().getResource(screen));
        Scene scene=new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
