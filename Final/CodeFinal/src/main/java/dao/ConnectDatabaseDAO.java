package dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class ConnectDatabaseDAO {
    private static String DB_URL =null;
    private static String USER_NAME =null;
    private static String PASSWORD =null;
    private static Connection connection=null;
    public static Connection getConnection() {

        if(connection!=null) return connection;
        try (InputStream input = new FileInputStream("src/main/resources/application.properties")) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            DB_URL=prop.getProperty("db.url");
            USER_NAME=prop.getProperty("db.username");
            PASSWORD=prop.getProperty("db.password");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(DB_URL, USER_NAME,PASSWORD);
            System.out.println("connect successfully!");
        } catch (Exception ex) {
            System.out.println("connect failure!");
            ex.printStackTrace();
        }
        return connection;
    }
}
