package controller;

import dao.ConnectDatabaseDAO;
import entity.Bike;
import entity.Station;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;


/**
 * @author dell
 *
 */
public class ManageBikeController {

	public static final String NUM_ALPHABET_REGEX = "[a-zA-Z_0-9\\-_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢ"
			+ "ẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈ" 
			+ "ỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$";
	public static final String NO_SPECIAL_REGEX = "^[a-zA-Z_0-9_\\,\\.\\/\\_\\- ]+$";
	
	private Connection connection = ConnectDatabaseDAO.getConnection();
	private ResultSet result;
	private String query;
	Pattern pattern;

	
	/**
	 * @return
	 */
	public List<Bike> getAllBike() {
		List<Bike> bikes = new ArrayList<Bike>();
		try {
			Statement statement = connection.createStatement();
			query = "SELECT * FROM bike";
			result = statement.executeQuery(query);
			while (result.next()) {
				Bike bike = new Bike();
				bike.setId(result.getInt("id"));
				bike.setBattery(result.getInt("battery"));
				bike.setStatus(result.getInt("status"));
				bike.setCost(result.getDouble("cost"));
				bike.setLicense(result.getString("license"));
				bike.setType(result.getInt("type"));
				bike.setStation(result.getInt("station"));
				bikes.add(bike);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return bikes;
	}
	public List<Station> getStations() throws SQLException {
		List<Station> stations = new ArrayList<>();
		String sql = "select * from station";

		PreparedStatement preStatement = null;
		try {
			preStatement = this.connection.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				Station sta = new Station();
				sta.setId(result.getInt("id"));
				sta.setStationname(result.getString("stationname"));
				sta.setStationaddress(result.getString("stationaddress"));
				sta.setNumberofemptydocks(result.getInt("numberofemptydocks"));
				stations.add(sta);
				sql = "Select count(*) from bike where station=" + sta.getId() + " and status=1";
				preStatement = this.connection.prepareStatement(sql);
				ResultSet rs = preStatement.executeQuery();
				rs.next();
				sta.setNumberofbikes(rs.getInt(1));
				rs.close();
			}
			result.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			preStatement.close();
		}
		return stations;
	}

	public void addBike(int type,int status, double cost, int battery,String license,int station) throws SQLException {

		query = "INSERT INTO bike(type,status,cost,battery,license,station)"
				+ " VALUES (?,?,?,?,?,?)";
		PreparedStatement statement=connection.prepareStatement(query);
		statement.setInt(1,type);
		statement.setInt(2,status);
		statement.setDouble(3,cost);
		statement.setInt(4,battery);
		statement.setString(5,license);
		statement.setInt(6,station);
		try {
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public boolean checkDuplicateName(List<Bike> bikelist, String nameOfBike) {
		System.out.println(nameOfBike + "\nList of Bike:");
		int numOfBikes = bikelist.size();
		Bike b;
		boolean result = true;
		for(int i = 0; i < numOfBikes; i++) {
			b = bikelist.get(i);
			System.out.println(b.getLicense());
			if (nameOfBike.equals(b.getLicense())) {
				result = false;
				break;
			}
		}
		System.out.println(result);
		return result;
	}
	
	
	/**

	 */
	public String processBikeInfo(String license, String cost) {
		String message = "";
		int errcount = 0;
		if (!validateLicense(license)) {
			message += "invalid license. ";
			errcount++;
		}
		if (!validateCost(cost)) {
			message += "invalid price. ";
			errcount++;
		}
		if (errcount != 0) {
			return message;
		} else
			return "valid_info";
	}

	public boolean validateName(String name) {
		boolean result = true;

		if (name != null && !name.isBlank()) {
			pattern = Pattern.compile(NUM_ALPHABET_REGEX);
			result = pattern.matcher(name).matches();
		} else
			result = false;

		return result;
	}

	public boolean validateLicense(String license) {
		boolean result = true;

		if (license != null && !license.isBlank()) {
			pattern = Pattern.compile(NO_SPECIAL_REGEX);
			result = pattern.matcher(license).matches();
		} else
			result = false;
		
		return result;
	}


	public boolean validateCost(String cost) {
		if (cost != null) {
			int c;
			try {
				c = Integer.parseInt(cost);
			} catch (NumberFormatException e) {
				return false;
			}
			if (c < 0)
				return false;
		} else
			return false;

		return true;
	}

}
