package view;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import controller.ManageBikeController;
import entity.Bike;
import entity.Station;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class AddBikeScreen implements Initializable {
	@FXML
	private TextField name;
	@FXML
	private ChoiceBox<String> type;
	@FXML
	private TextField weight;
	@FXML
	private TextField license;
	@FXML
	private DatePicker manufacturingdate;
	@FXML
	private TextField producer;
	@FXML
	private TextField cost;
	@FXML
	private ChoiceBox<String> station;

	private Stage stage;
	private Scene scene;
	private Parent root;
	
	String[] bikeTypes = { "1", "2", "3" };
	Alert a = new Alert(AlertType.ERROR);
	Alert b = new Alert(AlertType.INFORMATION);
	private HashMap<String,Integer> stationmap;
	private List<String> stationoption;

	ManageBikeController managebike = new ManageBikeController();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		type.getItems().addAll(bikeTypes);
		type.setValue("1");
		stationmap=new HashMap<>();
		stationoption=new ArrayList<>();
		try {
			for(Station station:managebike.getStations()) {
				stationmap.put(station.getStationname(),station.getId());
				stationoption.add(station.getStationname());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		station.getItems().addAll(stationoption);
	}
	
	public void showAlert(Alert a, int height, int width, String header, String message) {
		a.setResizable(true);
		a.setHeaderText(header);
		a.getDialogPane().setPrefSize(width, height);
		a.setContentText(message);
	}

	@FXML
	public void resetTextField(MouseEvent event) throws IOException{
		name.setText("");
		type.valueProperty().set(null);
		weight.setText("");
		license.setText("");
		manufacturingdate.getEditor().clear();
		producer.setText("");
		cost.setText("");
		station.valueProperty().set(null);
	}
	
	@FXML
	public void returnToPrevious(MouseEvent event) throws IOException, InterruptedException, SQLException {

		FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/bike_fxml/manageBike.fxml"));
		root = loader.load();

		// Get the source of the event and then cast it to a node
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	@FXML
	public void AddBikeToDb(MouseEvent event) throws IOException, SQLException {
		managebike.getStations();
		if (manufacturingdate.getValue() != null) {
			Date currDate = Calendar.getInstance().getTime();
			LocalDate bManuDate = manufacturingdate.getValue();
			Instant instant = Instant.from(bManuDate.atStartOfDay(ZoneId.systemDefault()));
			Date bDate = Date.from(instant);

			String message = managebike.processBikeInfo(name.getText(), weight.getText(), license.getText(), bDate,
					currDate, cost.getText());

			if (message == "valid_info") {
				if (managebike.checkDuplicateName(name.getText())) {
					String bName = name.getText();
					String bType = type.getValue();
					int bWeight = Integer.parseInt(weight.getText());
					String bLicense = license.getText();

					String bProducer = producer.getText();
					int bCost = Integer.parseInt(cost.getText());

					managebike.addBike(bName, stationmap.get(station.getValue()), bType, bWeight, bLicense, bDate, bProducer, bCost);
					showAlert(b, 230, 320, "Success", "Thêm xe thành công");
					b.show();
				} else {
					showAlert(a, 230, 320, "Thông tin không hợp lệ", "Tên xe đạp bị trùng lặp");
					a.show();
				}

			} else {
				showAlert(a, 230, 320, "Thông tin không hợp lệ", message);
				a.show();
			}
		} else {
			showAlert(a, 230, 320, "Thông tin không hợp lệ", "Cần thêm thông tin ngày (hoặc các thông tin khác) trước khi bấm thêm");
			a.show();
		}
	}

}
