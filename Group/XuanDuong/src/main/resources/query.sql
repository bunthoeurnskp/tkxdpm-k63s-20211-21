create database ecobike;
use ecobike;
drop table bike;
create table bikes(
                      id int auto_increment,
                      station_id int,
                      name nvarchar(50) unique,
                      type int,
                      weight int,
                      license nvarchar(50),
                      manufacturingdate date,
                      producer nvarchar(50),
                      cost decimal,
                      primary key(id)
);
create table stations(
                         id int auto_increment,
                         stationname nvarchar(100),
                         stationaddress nvarchar(300),
                         numberofempltydocks int,
                         coord_x int,
                         coord_y int,
                         primary key (id)
);