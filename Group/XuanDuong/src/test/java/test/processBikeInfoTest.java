package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ManageBikeController;

class processBikeInfoTest {

	private ManageBikeController manageBike;
	@BeforeEach
	void setUp() throws Exception {
		manageBike = new ManageBikeController();
	}

	@ParameterizedTest
	@CsvSource({
		"xe đạp #$@, 100 , ad-149 , 11/07/2021, 11/11/2021, 10000,'Tên không hợp lệ. '",
		"xe đạp 1-a, ag1f, ad-149 , 11/07/2021, 11/11/2021, 10000,'Khối lượng không hợp lệ. '",
		"xe đạp 1  , 100 ,        , 11/07/2021, 11/11/2021, 10000,'Biển số xe không hợp lệ. '",
		"xe đạp 1  , 100 , ad-1 49,           , 11/11/2021, 10000,'Ngày sản xuất không hợp lệ. '",
		"xe đạp 1  , 100 , ad-149 , 11/07/2021, 11/11/2021, 100dg,'Gía thành không hợp lệ. '",
		"xe đạp 1  , 100 , ad-149 , 11/07/2021, 11/11/2021, 10000,'valid_info'"
	})

	void test(String name, String weight, String license, Date date, Date currDate, String cost, String expect) {
		String isValid = manageBike.processBikeInfo(name, weight, license, date, currDate, cost);
		assertEquals(isValid, expect);
	}
	
	@ParameterizedTest
	@CsvSource({
		"11/11/2022, 11/11/2021, false",
		"10/11/2021, 11/11/2021, true",
		"          , 11/11/2021, false",
	})

	void testDate(Date date, Date currDate, boolean expect) {
		boolean isValid = manageBike.validateDate(date, currDate);
		assertEquals(isValid, expect);
	}
	
	@ParameterizedTest
	@CsvSource({
		"xe dap 1, true",
		"xe đạp 1, true",
		"()ADa, false",
		"() xe 1, false",
		"'     ', false",
		",false"
	})

	void testName(String name, boolean expect) {
		boolean isValid = manageBike.validateName(name);
		assertEquals(isValid, expect);
	}

	@ParameterizedTest
	@CsvSource({
		"ab2-c86 m32, true",
		"$!12-bsd, false",
		"'     ', false",
		",false"
	})

	void testLicense(String license, boolean expect) {
		boolean isValid = manageBike.validateLicense(license);
		assertEquals(isValid, expect);
	}
	
	@ParameterizedTest
	@CsvSource({
		"123, true",
		"-123, false",
		"asdf1, false",
		"12 42, false",
		"'     ', false",
		",false"
	})

	void testWeight(String weight, boolean expect) {
		boolean isValid = manageBike.validateWeight(weight);
		assertEquals(isValid, expect);
	}
	
	@ParameterizedTest
	@CsvSource({
		"12300, true",
		"-1230, false",
		"asdf1, false",
		"12 42, false",
		"'     ', false",
		",false"
	})

	void testCost(String cost, boolean expect) {
		boolean isValid = manageBike.validateCost(cost);
		assertEquals(isValid, expect);
	}
}
