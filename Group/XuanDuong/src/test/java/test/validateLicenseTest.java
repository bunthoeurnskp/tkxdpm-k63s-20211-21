package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ManageBikeController;

class validateLicenseTest {

	private ManageBikeController manageBike;
	@BeforeEach
	void setUp() throws Exception {
		manageBike = new ManageBikeController();
	}

	@ParameterizedTest
	@CsvSource({
		"ab2-c86 m32, true",
		"$!12-bsd, false",
		"'     ', false",
		",false"
	})

	void test(String license, boolean expect) {
		boolean isValid = manageBike.validateLicense(license);
		assertEquals(isValid, expect);
	}

}
