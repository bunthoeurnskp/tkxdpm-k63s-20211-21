import dao.ConnectDatabaseDAO;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.Connection;


public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Connection connection=ConnectDatabaseDAO.getConnection();
        //tạo cửa sổ mới và load fxml file

        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/ManageStation2.fxml"));
        Scene scene = new Scene(root, 720, 480);
        scene.getStylesheets().add(getClass().getClassLoader().getResource("css/styles.css").toExternalForm());
        primaryStage.setTitle("Ecobike");
        primaryStage.setResizable(true);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    public static void main(String[] args) throws IOException {
        launch(args);
    }
}