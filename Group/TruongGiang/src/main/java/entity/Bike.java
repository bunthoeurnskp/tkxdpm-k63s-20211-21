package entity;


import java.text.DecimalFormat;

public class Bike {
    private int stt;
    private int id;
    private int type;
    private int status;
    private double cost;
    private int battery;
    private String license;
    private int station;
    DecimalFormat formatter = new DecimalFormat("###,###,###");
    public Bike(int stt, int id, int type, int status, double cost, int battery, String license) {
        super();
        this.stt = stt;
        this.id = id;
        this.type = type;
        this.status = status;
        this.cost = cost;
        this.battery = battery;
        this.license = license;
    }
    public Bike() {
        super();
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public double getCost() {
        return cost;
    }
    public void setCost(double cost) {
        this.cost = cost;
    }
    public int getBattery() {
        return battery;
    }
    public void setBattery(int battery) {
        this.battery = battery;
    }
    public String getLicense() {
        return license;
    }
    public void setLicense(String license) {
        this.license = license;
    }
    public int getStt() {
        return stt;
    }
    public void setStt(int stt) {
        this.stt = stt;
    }

    public String formatPrice(Double price) {

        return formatter.format(price)+"";
    }

    public int getStation() {
        return station;
    }

    public void setStation(int station) {
        this.station = station;
    }

    public String getRent1() {
        double price = 10000;
        if( this.type == 2 || this.type == 3) {
            price *=1.5;
        }
        return formatter.format(price);
    }
    public String getRent2() {
        double price = 3000;
        if( this.type == 2 || this.type == 3) {
            price *=1.5;
        }
        return formatter.format(price);
    }

}

