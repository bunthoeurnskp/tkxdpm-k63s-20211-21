package controller;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ManageStationControllerTestAddStation {
    private ManageStationController manageStationController;
    @BeforeEach
    public void setUp() throws Exception {
        manageStationController=new ManageStationController();
    }
    @ParameterizedTest
    @CsvSource({
            ",Dai co viet,0,Ten khong duoc de trong",
            "Nuoc ngam,, 0,Dia chi khong duoc de trong",
            "Giap bat,Hoang mai,aa,Slot trong phai la so nguyen lon hon 0",
            "Giap bat,Hoang mai,30,SUCCESS"
    })
    public void test(String name,String address,String slots,String expected) {
        if(address==null) address="";
        if(name==null) name="";
        String result=manageStationController.addStation(name,address,slots);
        assertEquals(expected,result);
    }

}

