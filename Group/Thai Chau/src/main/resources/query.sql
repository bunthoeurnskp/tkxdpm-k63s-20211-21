 create database ecobike;
use ecobike;
-- drop table ecobike.bikes;
create table bikes (
                      id int auto_increment,
                      station_id int,
                      name nvarchar(50) unique,
                      type int,
                      weight int,
                      license nvarchar(50),
                      manufacturingdate date,
                      producer nvarchar(50),
                      cost decimal,
                      primary key(id)
);
create table stations (
                         id int auto_increment,
                         stationname nvarchar(100),
                         stationaddress nvarchar(300),
                         numberofempltydocks int,
                         coord_x int,
                         coord_y int,
                         primary key (id)
);
--drop table ecobike.rent_infos;
create table ecobike.rent_infos (
                      id int auto_increment,
                      bike_id int not null,
                      bike_type int not null,
                      rent_time datetime not null,
                      cost decimal not null,
                      primary key(id)
);

insert into ecobike.rent_infos(bike_id,rent_time,cost) values (1,'2021-12-26 00:59:59',400000);
insert into ecobike.bikes(station_id,name,type,weight,license,manufacturingdate,producer,cost) values (1,'thaichau',0,15,'abcxyz','2020-12-05 10:30:21','bach khoa',400000);