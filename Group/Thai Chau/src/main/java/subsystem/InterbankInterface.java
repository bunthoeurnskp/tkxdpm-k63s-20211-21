package subsystem;

import exception.PaymentException;
import exception.UnrecognizedException;
import entity.CreditCard;
import entity.PaymentTransaction;

public interface InterbankInterface {

    public abstract PaymentTransaction payOrder(CreditCard card, int amount, String contents)
            throws PaymentException, UnrecognizedException;

    public abstract PaymentTransaction refund(CreditCard card, int amount, String contents)
            throws PaymentException, UnrecognizedException;

}
