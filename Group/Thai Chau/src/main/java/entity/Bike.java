package entity;

public class Bike {
    private int stt;
    private int id;
    private int type;
    private int status;
    private long cost;
    private int battery;
    private String license;
    public Bike(int stt, int id, int type, int status, long cost, int battery, String license) {
        super();
        this.stt = stt;
        this.id = id;
        this.type = type;
        this.status = status;
        this.cost = cost;
        this.battery = battery;
        this.license = license;
    }
    public Bike() {
        super();
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public long getCost() {
        return cost;
    }
    public void setCost(long cost) {
        this.cost = cost;
    }
    public int getBattery() {
        return battery;
    }
    public void setBattery(int battery) {
        this.battery = battery;
    }
    public String getLicense() {
        return license;
    }
    public void setLicense(String license) {
        this.license = license;
    }

    public int getStt() {
        return stt;
    }

    public void setStt(int stt) {
        this.stt = stt;
    }
}
