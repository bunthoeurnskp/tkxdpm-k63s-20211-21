package entity;

import java.util.ArrayList;
import java.util.List;

public class Station {
    int id;
    String stationname;
    String stationaddress;
    int numberofemptydocks;
    int numberofbikes;
    List<Bike> bikes;

    public Station() {
    }

    public Station(String stationname, String stationaddress, int numberofemptydocks) {
        this.stationname = stationname;
        this.stationaddress = stationaddress;
        this.numberofemptydocks=numberofemptydocks;
        this.bikes = new ArrayList<>();
    }
    public void addBike(Bike bike){
        bikes.add(bike);
    }

    public int getNumberofemptydocks() {
        return numberofemptydocks;
    }

    public void setNumberofemptydocks(int numberofemptydocks) {
        this.numberofemptydocks = numberofemptydocks;
    }

    public void setBikes(List<Bike> bikes) {
        this.bikes = bikes;
    }

    public List<Bike> getBikes() {
        return bikes;
    }

    public int getNumberofbikes() {
        return numberofbikes;
    }

    public void setNumberofbikes(int numberofbikes) {
        this.numberofbikes = numberofbikes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStationname() {
        return stationname;
    }

    public void setStationname(String stationname) {
        this.stationname = stationname;
    }

    public String getStationaddress() {
        return stationaddress;
    }

    public void setStationaddress(String stationaddress) {
        this.stationaddress = stationaddress;
    }


    @Override
    public String toString() {
        return "Station{" +
                "id=" + id +
                ", stationname='" + stationname + '\'' +
                ", stationaddress='" + stationaddress + '\'' +
                ", numberofemptydocks=" + numberofemptydocks +
                ", numberofbikes=" + numberofbikes +
                ", bikes=" + bikes +
                '}';
    }
}
