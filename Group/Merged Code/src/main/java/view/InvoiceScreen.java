package view;

import controller.ReturnBikeController;
import entity.Invoice;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class InvoiceScreen {
    private ReturnBikeController returnBikeController = new ReturnBikeController();
    private static int selectedbike;
    @FXML
    private Label bikeId;

    @FXML
    private Label type;

    @FXML
    private Label rentTime;

    @FXML
    private Label returnTime;

    @FXML
    private Label price;

    @FXML
    private Button confirm;

    @FXML
    private TextField bikeIdInput;

    @FXML
    private Button getInvoice;

    @FXML
    public void confirmInvoice(ActionEvent event) throws IOException {
        if(event.getSource()==confirm) changeScreen(event,"../fxml/Payment.fxml");
        else changeScreen(event,"../fxml/Invoice.fxml");
    }

    @FXML
    public void backButton(ActionEvent event) throws IOException {
        changeScreen(event,"../fxml/home.fxml");
    }

    @FXML
    public void getBikeId(ActionEvent event) throws IOException {
        if(event.getSource()==getInvoice) {
            if (bikeIdInput.getText().isEmpty()) createAlert("Bạn chưa điền mã xe!");
            else setInvoiceInfo(bikeIdInput.getText());
        }
    }
    public static int getSelectedbike(){
        return selectedbike;
    }
    public void setInvoiceInfo(String id) {
        ArrayList<String> info = returnBikeController.getInvoice(id);
        if(info.isEmpty()) {
            createAlert("Xe chưa được thuê hoặc không tồn tại!");
        } else {
            //Show on screen
            bikeId.setText(info.get(0));
            if (info.get(1).equals("1")) type.setText("Normal bike");
            else if (info.get(1).equals("2")) type.setText("Electric bike");
            else type.setText("Tandem bike");
            rentTime.setText(info.get(2));
            returnTime.setText(info.get(3));
            price.setText(info.get(4));
            selectedbike=Integer.parseInt(info.get(0));
        }
    }

    public void changeScreen(ActionEvent event,String screen) throws IOException {
        Stage stage=(Stage)((Node)event.getSource()).getScene().getWindow();
        Parent root= FXMLLoader.load(getClass().getResource(screen));
        Scene scene=new Scene(root, 1240, 768);
        scene.getStylesheets().add(getClass().getResource("../css/styles.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    private Alert createAlert(String str) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("");
        alert.setContentText(str);
        alert.showAndWait();
        return alert;
    }
}
