package view;

import controller.RentBikeController;
import entity.Bike;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class RentBikeScreen {
    @FXML
    private Button back;
    @FXML
    private Button confirm;
    @FXML
    private TextField bikecodeinput;
    @FXML
    private Label name,code,pin;
    private RentBikeController rentBikeController=new RentBikeController();
    private Bike selected=null;
    @FXML
    public void search(ActionEvent event){
        if(bikecodeinput.getText().equals("")){
            showAlert("Dien ma xe");
        }
        try{
            Bike bike=rentBikeController.getBikeByID(Integer.parseInt(bikecodeinput.getText()));
            if(bike==null) showAlert("khong tim thay xe");
            else {
                selected=bike;
                name.setText(bike.getType()==1?"Xe đạp thường":(bike.getType()==2?"Xe điện":"Xe đạp đôi"));
                if(bike.getType()==2) pin.setText(bike.getBattery()+"%");
                code.setText(bike.getLicense());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @FXML
    public void rent(ActionEvent event) throws IOException {
        rentBikeController.rentBike(selected);
        showAlert("Thuê xe thành công");
        Stage stage=(Stage)((Node)event.getSource()).getScene().getWindow();
        Parent root= FXMLLoader.load(getClass().getResource("../fxml/home.fxml"));
        Scene scene=new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    public void showAlert(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
    @FXML
    public void changepage(ActionEvent event) throws IOException {
        //chuyển đổi trang


        Stage stage=(Stage)((Node)event.getSource()).getScene().getWindow();
        Parent root;
        if(event.getSource()==back)
        {
            root= FXMLLoader.load(getClass().getResource("../fxml/home.fxml"));
        }
        else
        {
            root=FXMLLoader.load(getClass().getResource("../fxml/RentBike.fxml"));
        }



        Scene scene=new Scene(root);
        stage.setScene(scene);
        stage.show();

    }
}
