package view;

import controller.PaymentController;
import controller.ReturnBikeController;
import entity.Invoice;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Map;

public class PaymentScreen {
    @FXML
    private TextField cardId;
    @FXML
    private TextField name;
    @FXML
    private TextField expirationDate;
    @FXML
    private TextField cvv;
    @FXML
    private Button confirm;
    @FXML
    private Button cancel;
    @FXML

    Invoice invoice = Invoice.Invoice();
    ReturnBikeController returnBikeController=new ReturnBikeController();
    public void confirmPayment(ActionEvent event) throws IOException {
        String contents = "pay order";
        PaymentController ctrl = new PaymentController();
//        Map<String, String> response = ctrl.payOrder(invoice.price, contents, cardId.getText(), name.getText(),
//                expirationDate.getText(), cvv.getText());
        returnBikeController.returnBike(InvoiceScreen.getSelectedbike());
        showAlert("PAYMENT SUCCESSFUL!");
        changeScreen(event,"../fxml/home.fxml");
//        if(response.get("RESULT") == "PAYMENT SUCCESSFUL!") {
//            showAlert("PAYMENT SUCCESSFUL!");
//            changeScreen(event,"../fxml/home.fxml");
//        }
//        else {
//            showAlert(response.get("MESSAGE"));
//            changeScreen(event,"../fxml/Payment.fxml");
//        }
    }

    public void showAlert(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    @FXML
    public void Cancel(ActionEvent event) throws IOException {
        changeScreen(event,"../fxml/home.fxml");
    }

    public void changeScreen(ActionEvent event,String screen) throws IOException {
        Stage stage=(Stage)((Node)event.getSource()).getScene().getWindow();
        Parent root= FXMLLoader.load(getClass().getResource(screen));
        Scene scene=new Scene(root, 1240, 768);
        scene.getStylesheets().add(getClass().getResource("../css/styles.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }
}
