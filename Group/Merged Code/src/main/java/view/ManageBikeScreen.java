package view;

import controller.ManageBikeController;
import entity.Bike;
import entity.Station;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

public class ManageBikeScreen implements Initializable{

	@FXML
	private TableView<Bike> table;
	@FXML
	private TableColumn<Bike, String> type;
	@FXML
	private TableColumn<Bike, String> license;
	@FXML
	private TableColumn<Bike, String> battery;
	@FXML
	private TableColumn<Bike, BigDecimal> cost;
	@FXML
	private TableColumn<Bike, String> station;
	
	ManageBikeController managebike = new ManageBikeController();
	List<Bike> bikes = new ArrayList<Bike>();
	private Stage stage;
	private Scene scene;
	private Parent root;
	private HashMap<Integer,String> stations;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
		stations=new HashMap<>();
		try {
			for(Station s:managebike.getStations()){
				stations.put(s.getId(),s.getStationname());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		type.setCellValueFactory(cellData->{
			String text = "Xe đạp thường";
			if (cellData.getValue().getType() == 2) {
				text = "Xe điện";
			} else if (cellData.getValue().getType() == 3) {
				text = "Xe đạp đôi";
			}
			return new SimpleStringProperty(text);
		});
    	license.setCellValueFactory(new PropertyValueFactory<Bike, String>("license"));
    	battery.setCellValueFactory(cellData->{
			String text = "";
			if (cellData.getValue().getBattery()!= -1) {
				text = cellData.getValue().getBattery()+" %";
			}
			return new SimpleStringProperty(text);
		});
    	cost.setCellValueFactory(new PropertyValueFactory<Bike, BigDecimal>("cost"));
		station.setCellValueFactory(cellData-> new SimpleStringProperty(stations.get(cellData.getValue().getStation())));
		System.out.println(managebike.getAllBike().get(1).toString());
    	table.getItems().setAll(managebike.getAllBike());
    }
	
	@FXML
	public void addBikeScreen(MouseEvent event)throws IOException, InterruptedException, SQLException{
		Stage stage=(Stage)((Node)event.getSource()).getScene().getWindow();
		Parent root= FXMLLoader.load(getClass().getResource("../fxml/addbike.fxml"));
		Scene scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	@FXML
	public void backtopre(ActionEvent event) throws IOException {
		Stage stage=(Stage)((Node)event.getSource()).getScene().getWindow();
		Parent root= FXMLLoader.load(getClass().getResource("../fxml/home.fxml"));
		Scene scene=new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
