package controller;

import dao.ConnectDatabaseDAO;
import entity.Bike;
import entity.Station;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RentBikeController {
    private final Connection connection;
    ManageBikeController manageBikeController=new ManageBikeController();
    public RentBikeController() {
        connection= ConnectDatabaseDAO.getConnection();
    }
    public Bike getBikeByID(int id) throws SQLException {

        PreparedStatement preStatement = null;
        try {
            preStatement = this.connection.prepareStatement("Select * from bike where status=1 and id="+id);
            ResultSet result = preStatement.executeQuery();
            if (result.next()) {

                Bike bike = new Bike();
                bike.setId(result.getInt("id"));
                bike.setType(result.getInt("type"));
                bike.setLicense(result.getString(4));
                bike.setBattery(result.getInt("battery"));
                return  bike;
            }
                else return null;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if(preStatement!=null) {
                preStatement.close();
            }
        }
        return null;
    }
    public void rentBike(Bike bike){
            try{
                String sql="insert into rent_infos(bike_id,bike_type,rent_time,cost) values " +
                        "(?,?,?,100000)";
                PreparedStatement statement=connection.prepareStatement(sql);
                statement.setInt(1,bike.getId());
                statement.setInt(2,bike.getType());
                statement.setTimestamp(3,new Timestamp(System.currentTimeMillis()));
                statement.executeUpdate();
                sql="update bike " +
                        " set status=? " +
                        "where id=?";
                statement=connection.prepareStatement(sql);
                statement.setInt(1,0);
                statement.setInt(2,bike.getId());
                statement.executeUpdate();
            }catch (Exception e){
                e.printStackTrace();

            }
    }
}
