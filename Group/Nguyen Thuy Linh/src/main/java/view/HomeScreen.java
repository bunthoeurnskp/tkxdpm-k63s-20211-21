package view;

import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import controller.HomeController;
import entity.Bike;
import entity.Station;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Pagination;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;

public class HomeScreen implements Initializable {
	@FXML
	private VBox leftComponent;
	@FXML
	private Label homeLabel;
	@FXML
	private SplitPane splitPane;

	private Button btnStation, btnTest, btnStationSearch;
	private BorderPane rightComponent;
	private static int btnActive = 1;
	private TextField txtStationSearch;
	private Pagination pagination;
	private GridPane gridPane;
	private TableView<Bike> table;
	
	private final int G_COLUMN = 3;
	private final int G_ROW = 3;
	private int page = 1;
	private int pageSize = G_COLUMN * G_ROW;
	private int maxPage = 1;
	private String textSearch;
	private int stationSelected = -1;
	private String stationSelectedName = "";
	private int type = 0;

	Pattern regex = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!-]");

	private List<Station> stations;
	private List<Bike> bikes;
	private HomeController homeController;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		homeController = new HomeController();
		rightComponent = new BorderPane();
		table = new TableView<>();
		addComponent(true);
		addVBox();
		activeButton();
	}

	private void getData() throws SQLException {
		this.stations = homeController.getStation(this.textSearch, this.page, this.pageSize);
		this.maxPage = (int) Math.ceil(homeController.getCountStation(this.textSearch) / (pageSize * 1.0));
	}

	private void getBikeData() throws SQLException {
		this.bikes = homeController.getBikes(this.textSearch, this.stationSelected, this.type);
	}

	public void addComponent(boolean isFirst) {
		if (!isFirst) {
			this.page = 1;
			this.textSearch = null;
			splitPane.getItems().remove(rightComponent);
			rightComponent.getChildren().clear();
		}
		switch (btnActive) {
		case 1:
			addHomeView();
			break;
		case 2:
			addTestView();
			break;
		default:
			break;
		}
		splitPane.getItems().add(rightComponent);
		splitPane.setDividerPositions(0.25);
	}

	private void addTestView() {
		Label testlb = new Label("sdfsdfsd");
		rightComponent.setTop(testlb);
	}

	private void addHomeView() {
		this.page = 1;
		try {
			getData();
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		txtStationSearch = new TextField();
		txtStationSearch.setFont(Font.font("Segoe UI", 20));
		txtStationSearch.setPromptText("Tìm kiếm bãi xe");

		btnStationSearch = new Button("Tìm kiếm");
		btnStationSearch.setFont(Font.font("Segoe UI", 20));
		btnStationSearch.setDisable(true);
		btnStationSearch.setOnMouseClicked(e -> {
			if (regex.matcher(this.textSearch).find()) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Lỗi");
				alert.setContentText("Từ khoá tìm kiếm có chứa kí tự đặc biệt.");
				alert.setHeaderText(null);
				alert.showAndWait();
				return;
			}
			this.page = 1;
			try {
				getData();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			pagination.setPageCount(this.maxPage);
			pagination.setCurrentPageIndex(0);
			addGridView();
		});

		txtStationSearch.textProperty().addListener((observable, oldValue, newValue) -> {
			this.textSearch = newValue;
			if (newValue.length() == 0) {
				btnStationSearch.setDisable(true);
				this.page = 1;
				try {
					getData();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				pagination.setPageCount(this.maxPage);
				pagination.setCurrentPageIndex(0);
				addGridView();
			} else {
				btnStationSearch.setDisable(false);
			}
		});

		pagination = new Pagination(this.maxPage);
		pagination.setPadding(new Insets(0, 0, 10, 0));
		pagination.currentPageIndexProperty().addListener((obs, oldIndex, newIndex) -> {
			String page = newIndex + "";
			this.page = Integer.parseInt(page) + 1;
			try {
				getData();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			addGridView();
		});

		BorderPane topPanel = new BorderPane();
		topPanel.setCenter(txtStationSearch);
		topPanel.setRight(btnStationSearch);
		BorderPane.setMargin(topPanel, new Insets(10, 20, 10, 20));

		gridPane = new GridPane();
		gridPane.setHgap(20);
		gridPane.setVgap(20);
		for (int i = 0; i < G_COLUMN; i++) {
			ColumnConstraints colConst = new ColumnConstraints();
			colConst.setPercentWidth(100.0 / G_COLUMN);
			gridPane.getColumnConstraints().add(colConst);
		}
		for (int i = 0; i < G_ROW; i++) {
			RowConstraints rowConst = new RowConstraints();
			rowConst.setPercentHeight(100.0 / G_ROW);
			gridPane.getRowConstraints().add(rowConst);
		}
		gridPane.setPadding(new Insets(20));
		addGridView();

		rightComponent.setTop(topPanel);
		rightComponent.setCenter(gridPane);
		rightComponent.setBottom(pagination);
	}

	private void addGridView() {
		gridPane.getChildren().clear();
		for (int i = 0; i < this.stations.size(); ++i) {

			BorderPane bd = new BorderPane();
			bd.setBorder(new Border(new BorderStroke(Color.rgb(229, 229, 229), BorderStrokeStyle.SOLID,
					CornerRadii.EMPTY, BorderWidths.DEFAULT)));
			bd.setPadding(new Insets(10));
			bd.getStyleClass().add("payload");
			Label lbName = new Label(this.stations.get(i).getName());
			lbName.setFont(Font.font("Segoe UI bold", 20));
			bd.setOnMouseEntered(e -> {
				TranslateTransition tt = new TranslateTransition(Duration.millis(250), bd);
				tt.setToY(-2);
				tt.play();
			});
			bd.setOnMouseExited(e -> {
				TranslateTransition tt = new TranslateTransition(Duration.millis(250), bd);
				tt.setToY(2);
				tt.play();
			});

			VBox boxCenter = new VBox();
			boxCenter.setAlignment(Pos.CENTER);

			Label lbAddress = new Label("Địa chỉ: " + this.stations.get(i).getAddress());
			lbAddress.setFont(Font.font("Segoe UI", 20));
			Label empty = new Label(
					this.stations.get(i).getSlotEmpty() > 0 ? this.stations.get(i).getSlotEmpty() + " có thể sử dụng"
							: "Bãi xe trống");
			empty.setFont(Font.font("Segoe UI", FontWeight.BOLD, 20));

			boxCenter.getChildren().add(lbAddress);
			boxCenter.getChildren().add(empty);

			Button btnDetail = new Button("Detail");
			btnDetail.getStyleClass().add("btn-detail");
			btnDetail.setFont(Font.font("Segoe UI bold", 20));
			int slotEmpty = this.stations.get(i).getSlotEmpty();
			int stationId = this.stations.get(i).getId();
			String name = this.stations.get(i).getName();
			btnDetail.setOnMouseClicked(e -> {
				if (slotEmpty == 0) {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Thông báo");
					alert.setContentText("Bãi xe đang trống. Vui lòng thử lại sau.");
					alert.setHeaderText(null);
					alert.showAndWait();
				} else {
					this.stationSelected = stationId;
					this.stationSelectedName = name;
					this.textSearch = null;
					this.type = 0;
					addBikeView();
				}
			});

			bd.setTop(lbName);
			bd.setCenter(boxCenter);
			bd.setBottom(btnDetail);
			BorderPane.setAlignment(lbName, Pos.CENTER);
			BorderPane.setAlignment(btnDetail, Pos.CENTER);

			if (i < 3) {
				gridPane.add(bd, i, 0);
			} else {
				gridPane.add(bd, i % G_COLUMN, i / G_ROW);
			}
		}
	}

	private void addBikeView() {
		try {
			getBikeData();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		txtStationSearch = new TextField();
		txtStationSearch.setFont(Font.font("Segoe UI", 20));
		txtStationSearch.setPromptText("Tìm kiếm xe");

		txtStationSearch.textProperty().addListener((observable, oldValue, newValue) -> {
			this.textSearch = newValue;
			if (newValue.length() == 0) {
				try {
					getBikeData();
					addTableView();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

		VBox vBox = new VBox();
		vBox.getStyleClass().add("back-image");
		File file = new File("assets/images/back.png");
		Image image = new Image(file.toURI().toString());
		ImageView imageView = new ImageView(image);
		imageView.setFitWidth(40);
		imageView.setFitHeight(40);
		vBox.setOnMouseClicked(e -> {
			this.textSearch = null;
			addHomeView();
		});
		vBox.getChildren().add(imageView);

		SplitMenuButton splitMenuButton = new SplitMenuButton();
		splitMenuButton.setText("Tìm kiếm");

		MenuItem choice1 = new MenuItem("Tất cả");
		MenuItem choice2 = new MenuItem("Xe thường");
		MenuItem choice3 = new MenuItem("Xe điện");
		MenuItem choice4 = new MenuItem("Xe đạp đôi");

		choice1.setOnAction((e) -> {
			this.type = 0;
			try {
				getBikeData();
				addTableView();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		});
		choice2.setOnAction((e) -> {
			this.type = 1;
			try {
				getBikeData();
				addTableView();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		});
		choice3.setOnAction((e) -> {
			this.type = 2;
			try {
				getBikeData();
				addTableView();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		});
		choice4.setOnAction((e) -> {
			this.type = 3;
			try {
				getBikeData();
				addTableView();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		});

		splitMenuButton.getItems().addAll(choice1, choice2, choice3, choice4);
		splitMenuButton.setOnAction((e) -> {
			if (Objects.isNull(this.textSearch) || this.textSearch.length() == 0) {
				return;
			}
			if (regex.matcher(this.textSearch).find()) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Lỗi");
				alert.setContentText("Từ khoá tìm kiếm có chứa kí tự đặc biệt");
				alert.setHeaderText(null);
				alert.showAndWait();
				return;
			}
			try {
				getBikeData();
				addTableView();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		});
		splitMenuButton.setFont(Font.font("Segoe UI", 20));

		VBox vBoxRight = new VBox();
		vBoxRight.getChildren().addAll(btnStationSearch, splitMenuButton);

		Label stationName = new Label(this.stationSelectedName);
		stationName.setPadding(new Insets(10));
		stationName.setFont(Font.font("Segoe UI bold", 20));

		BorderPane topPanel = new BorderPane();
		topPanel.setCenter(txtStationSearch);
		topPanel.setRight(splitMenuButton);
		topPanel.setLeft(vBox);
		topPanel.setBottom(stationName);
		BorderPane.setAlignment(stationName, Pos.CENTER);
		BorderPane.setMargin(topPanel, new Insets(10, 20, 0, 20));

		addTableView();

		BorderPane.setMargin(table, new Insets(10, 20, 20, 20));

		rightComponent.getChildren().clear();
		rightComponent.setCenter(table);
		rightComponent.setTop(topPanel);
	}

	private void addTableView() {
		table.getItems().clear();
		table.setPlaceholder(new Label("Không tìm thấy kết quả"));
		TableColumn<Bike, Integer> col1 = new TableColumn<>("STT");
		col1.setCellValueFactory(new PropertyValueFactory<>("stt"));
		TableColumn<Bike, String> col2 = new TableColumn<>("Loại xe");
		col2.setCellValueFactory(new PropertyValueFactory<>("type"));
		TableColumn<Bike, String> col3 = new TableColumn<>("Biển số");
		col3.setCellValueFactory(new PropertyValueFactory<>("license"));
		TableColumn<Bike, String> col4 = new TableColumn<>("Giá xe (VNĐ)");
		col4.setCellValueFactory(new PropertyValueFactory<>("cost"));
		TableColumn<Bike, String> col5 = new TableColumn<>("Pin (%)");
		col5.setCellValueFactory(new PropertyValueFactory<>("battery"));
		TableColumn<Bike, String> col6 = new TableColumn<>("Giá thuê (VNĐ)");
		TableColumn<Bike, String> col7 = new TableColumn<>("10-30 phút đầu");
		TableColumn<Bike, String> col8 = new TableColumn<>("15 phút tiếp theo");
		col7.prefWidthProperty().bind(col6.widthProperty().multiply(0.5));
		col8.prefWidthProperty().bind(col6.widthProperty().multiply(0.5));
		col6.getColumns().addAll(col7, col8);
		table.getColumns().addAll(col1, col2, col3, col4, col5, col6);

		col1.prefWidthProperty().bind(table.widthProperty().multiply(0.05));
		col2.prefWidthProperty().bind(table.widthProperty().multiply(0.2));
		col3.prefWidthProperty().bind(table.widthProperty().multiply(0.2));
		col4.prefWidthProperty().bind(table.widthProperty().multiply(0.2));
		col5.prefWidthProperty().bind(table.widthProperty().multiply(0.1));
		col6.prefWidthProperty().bind(table.widthProperty().multiply(0.25));
		table.getColumns().forEach(e -> {
			e.setReorderable(false);
			e.setResizable(false);
			e.setSortable(false);
		});
		col2.setCellValueFactory(cellData -> {
			String text = "Xe đạp thường";
			if (cellData.getValue().getType() == 2) {
				text = "Xe điện";
			} else if (cellData.getValue().getType() == 3) {
				text = "Xe đạp đôi";
			}
			return new SimpleStringProperty(text);
		});
		col4.setCellValueFactory(cellData -> {
			return new SimpleStringProperty(cellData.getValue().formatPrice(cellData.getValue().getCost()));
		});
		col5.setCellValueFactory(cellData -> {
			String text = cellData.getValue().getBattery() > -1 ? cellData.getValue().getBattery() + "%" : "";
			return new SimpleStringProperty(text);
		});
		col7.setCellValueFactory(cellData -> {
			return new SimpleStringProperty(cellData.getValue().getRent1());
		});
		col8.setCellValueFactory(cellData -> {
			return new SimpleStringProperty(cellData.getValue().getRent2());
		});
		table.setEditable(false);
		for (int i = 0; i < this.bikes.size(); ++i) {
			table.getItems().add(this.bikes.get(i));
		}
		table.setOnMouseClicked(e -> {
			Bike bikeSelected = this.bikes.get(table.getSelectionModel().getSelectedIndex());
			if (bikeSelected.getType() == 2 && bikeSelected.getBattery() == 0) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setHeaderText(null);
				alert.setTitle("Lỗi");
				alert.setContentText("Xin lỗi xe đang trong tình trạng hết pin. Vui lòng lựa chọn xe khác");
				alert.showAndWait();
				return;
			}
			System.out.println(bikeSelected.getId());
			System.out.println(bikeSelected.getLicense());
		});

	}

	public void addVBox() {
		btnStation = new Button("Bãi xe");
		btnStation.setPrefSize(200, 40);
		btnStation.setFont(Font.font("Segoe UI", 20));

		btnTest = new Button("Test");
		btnTest.setPrefSize(200, 40);
		btnTest.setFont(Font.font("Segoe UI", 20));

		btnStation.setOnMouseClicked(e -> {
			btnActive = 1;
			activeButton();
			addComponent(false);
		});

		btnTest.setOnMouseClicked(e -> {
			btnActive = 2;
			activeButton();
			addComponent(false);
		});

		leftComponent.setSpacing(20);
		leftComponent.getChildren().add(btnStation);
		leftComponent.getChildren().add(btnTest);
	}

	public void activeButton() {
		switch (btnActive) {
		case 1:
			this.btnStation.getStyleClass().add("btn-active");
			this.btnTest.getStyleClass().remove("btn-active");
			break;
		case 2:
			this.btnStation.getStyleClass().remove("btn-active");
			this.btnTest.getStyleClass().add("btn-active");
			break;
		default:
			this.btnStation.getStyleClass().add("btn-active");
			break;
		}
	}
}
