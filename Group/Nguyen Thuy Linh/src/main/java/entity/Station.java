package entity;

import java.util.List;

public class Station {
	private int id;
	private String name;
	private String address;
	private int slotEmpty;
	private int totalBikes;
	List<Bike> bikes;

	public Station(int id, String name, String address, int slotEmpty, List<Bike> bikes, int totalBikes) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.slotEmpty = slotEmpty;
		this.bikes = bikes;
		this.totalBikes = totalBikes;
	}

	public Station() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getSlotEmpty() {
		return slotEmpty;
	}

	public void setSlotEmpty(int slotEmpty) {
		this.slotEmpty = slotEmpty;
	}

	public List<Bike> getBikes() {
		return bikes;
	}

	public void setBikes(List<Bike> bikes) {
		this.bikes = bikes;
	}

	public int getTotalBikes() {
		return totalBikes;
	}

	public void setTotalBikes(int totalBikes) {
		this.totalBikes = totalBikes;
	}
	
}
