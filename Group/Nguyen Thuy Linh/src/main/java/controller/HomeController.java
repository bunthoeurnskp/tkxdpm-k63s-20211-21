package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dao.ConnectDatabaseDAO;
import entity.Bike;
import entity.Station;

public class HomeController {

	private Connection conn;

	public HomeController() {
		super();
		this.conn = ConnectDatabaseDAO.getConnection();
	}

	public List<Station> getStation(String textSearch, int page, int pageSize) throws SQLException {
		List<Station> stations = new ArrayList<>();
		String sql = "";
		if (Objects.isNull(textSearch)) {
			sql = "Select * from station order by name asc limit " + (page - 1) * pageSize + "," + pageSize;
		} else {
			sql = "Select * from station where name like \"%" + textSearch + "%\" or address like \"%" + textSearch
					+ "%\" order by name asc limit " + (page - 1) * pageSize + "," + pageSize;
		}
		PreparedStatement preStatement = null;
		try {
			preStatement = this.conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				Station sta = new Station();
				sta.setId(result.getInt("id"));
				sta.setName(result.getString("name"));
				sta.setAddress(result.getString("address"));
				stations.add(sta);
				sql = "Select count(*) from bike where station=" + sta.getId() + " and status=1";
				preStatement = this.conn.prepareStatement(sql);
				ResultSet rs = preStatement.executeQuery();
				rs.next();
				sta.setSlotEmpty(rs.getInt(1));
				rs.close();
			}
			result.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			preStatement.close();
		}
		return stations;
	}

	public List<Bike> getBikes(String textSearch, int stationId, int type) throws SQLException {
		List<Bike> bikes = new ArrayList<>();
		String sql = "";
		if (Objects.isNull(textSearch)) {
			sql = "select * from bike where status=1 and station=" + stationId;
		} else {
			sql = "select * from bike where license like \"%"+textSearch+"%\" and status=1 and station=" + stationId;
		}
		if(type != 0) {
			sql += " and type="+ type + " order by license asc";
		}
		else {
			sql += " order by type asc";
		}
		PreparedStatement preStatement = null;
		try {
			preStatement = this.conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			int i=1;
			while (result.next()) {
				Bike bike = new Bike();
				bike.setStt(i++);
				bike.setId(result.getInt("id"));
				bike.setBattery(result.getInt("battery"));
				bike.setStatus(result.getInt("status"));
				bike.setCost(result.getDouble("cost"));
				bike.setLicense(result.getString("license"));
				bike.setType(result.getInt("type"));
				bikes.add(bike);
			}
			result.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			preStatement.close();
		}
		return bikes;
	}

	public int getCountStation(String textSearch) throws SQLException {
		int count = 0;
		String sql = "";
		if (Objects.isNull(textSearch)) {
			sql = "Select count(*) from station";
		} else {
			sql = "Select count(*) from station where name like \"%" + textSearch + "%\" or address like \"%"
					+ textSearch + "%\"";
		}
		PreparedStatement preStatement = null;
		try {
			preStatement = this.conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			result.next();
			count = result.getInt(1);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			preStatement.close();
		}
		return count;
	}

}
